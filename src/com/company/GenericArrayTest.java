package com.company;

import java.util.ArrayList;

public class GenericArrayTest {
    public static void main(String[] args) {
        // Generic array
        // menggunakan generic array mempermudah pengelolaan isi
        ArrayList<Employee> list = new ArrayList<Employee>();
        ArrayList<Integer> list2 = new ArrayList<Integer>();

        // set nilai
        for (int i = 0; i < 5; i++) {
            Employee e = new Employee();
            e.setId(i + 1);
            e.setFullName("Employee " + (i + 1));

            // insert
            list.add(e);
        }

        // liat isi array
        System.out.println(list);
        for (Employee emp:list) {
            System.out.println(emp.getId() + " : " + emp.getFullName());
        }
    }
}
