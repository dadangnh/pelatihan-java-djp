package com.company;


public class MethodTest {
    public static void main(String[] args) {
        foo();

        int hasil = jumlah(10, 9);
        System.out.println("Jumlahnya: " + hasil);
        int hasil2 = jumlah(10);
        System.out.println("Kuadratnya: " + hasil2);
    }

    public static void foo() { // penulisan menggunakan Camel
        System.out.println("foo() was called");
    }

    public static int jumlah(int a, int b) {
        return a + b;
    }

    // overloading method: yg namanya sama tapi melakukan sesuatu yang berbeda dengan parameter yang berbeda
    public static int jumlah(int a) {
        return a * a;
    }

    public static void saveData(String str) { // penulisan method --> Camel
        System.out.println(str);
        System.out.println("saveData() was called");
    }


}
