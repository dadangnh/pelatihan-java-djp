package com.company;

// Library untuk menerima input dari keyboard
import java.util.Scanner;

public class MyTesting {
    public static void main(String[] args) {
        System.out.println("Hello MyTesting");

        // Untuk menerima input object (inisiasi object scanner)
        Scanner scanner = new Scanner(System.in);

        System.out.print("Input bilangan 1: ");
        Integer n = scanner.nextInt();
        System.out.print("Input bilangan 2: ");
        Integer m = scanner.nextInt();

        int jumlah = n + m;

        System.out.println("Hasil jumlah : " + jumlah);
    }
}