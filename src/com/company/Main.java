package com.company;

public class Main {

    public static void main(String[] args) {
        // write your code here

        System.out.println("Hello java");

        // Variabel
        // tipe_data nama_variabels;

        String str = "hello";
        Integer n = 10;
        double m = 3.87;
        Boolean isValid = true;

        System.out.println(str);
        System.out.println(n);
        System.out.println(m);
        System.out.println(isValid);

        System.out.println("Hasil: " + str + " " + n + " " + isValid);

        // Coba parsing
        String num = "10";

        // operasi matematika
        Integer nm = n + 10;
        // contoh code salah
//        Integer h = 8 - num;
        Integer h = 8 - Integer.parseInt(num);
        System.out.println(nm);
        System.out.println(h);
    }
}
