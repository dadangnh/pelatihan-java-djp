package com.company;

import java.util.Scanner;

public class TaxAppVersiKu {
    public static void main(String[] args) {
        // trigger stop, normalnya tidak, artinya aplikasi jalan terus sampai user pilih berhenti
        String stopAplikasi = "T";

        // inisiasi class scanner
        Scanner scanner = new Scanner(System.in);

        // jalankan aplikasi selama belum di stop user
        while (stopAplikasi.equals("T")) {
            // tampilkan pesan pembuka
            System.out.println("==================================================");
            System.out.println("Selamat datang di simulasi perhitungan pajak");
            System.out.println("==================================================");
            System.out.println("Tarif Pajak: A = 10%, B = 25%, C = 50%");
            System.out.println("==================================================");

            // setting total pajak yang dibayarkan
            double totalPajak = 0;

            // trigger untuk perhitungan baru, normalnya Ya, artinya mengulang pajaknya dari 0
            String perhitunganBaru = "Y";

            // mulai aplikasi ketika pajaknya 0
            while (perhitunganBaru.equals("Y")) {
                // setting pajak total selama loop ini
                double currentTotal = 0;

                // terima user input
                System.out.print("Masukkan DPP/ nilai transaksi: ");
                Integer dpp = scanner.nextInt();
                String tipePajak = "";

                // validasi tipe pajak
                do {
                    System.out.print("Tipe pajak [A, B, C]: ");
                    tipePajak = scanner.next();
                } while (!(tipePajak.equals("A") || tipePajak.equals("B") || tipePajak.equals("C")));

                // logic perhitungan
                switch (tipePajak) {
                    case "A":
                        currentTotal = dpp * 0.1;
                        break;
                    case "B":
                        currentTotal = dpp * 0.25;
                        break;
                    case "C":
                        currentTotal = dpp * 0.5;
                        break;
                }

                // tampilkan pajak yang dibayarkan
                System.out.println(String.format("Pajak Anda: %.2f", currentTotal));

                // masukkan ke total pajak
                totalPajak = totalPajak + currentTotal;

                // validasi apakah akan menambahkan transaksi lagi atau tidak, untuk menghentikan loop
                do {
                    System.out.print("Apakah transaksi lagi [Y, T]: ");
                    perhitunganBaru = scanner.next();
                } while (!(perhitunganBaru.equals("Y") || perhitunganBaru.equals("T")));

            }

            // Informasikan total pajak yang dibayar
            System.out.println("==================================================");
            System.out.println(String.format("Total Pajak yang harus dibayar: %.2f",totalPajak));

            // ketika total pajaknya lebih dari 500 ribu, dapat diskon
            if (totalPajak > 500000) {
                System.out.println("Anda dapat bonus pajak");
            } else {
                System.out.println("Anda tidak mendapatkan bonus pajak");
            }
            System.out.println("==================================================");

            // validasi exit aplikasi
            do {
                System.out.print("Apakah and mau keluar [Y, T]: ");
                stopAplikasi = scanner.next();
            } while (!(stopAplikasi.equals("Y") || stopAplikasi.equals("T")));

        }

    }

}

// logic aplikasi:
// selama user belum stop, aplikasi jalan
// user diberi kesempatan untuk menambah transaksi