package com.company;

public class ConditionalTest {
    public static void main(String[] args) {
        // Conditional

        int n = 10;
        if (n > 5) {
            System.out.println("Nilai n > 5");
        } else {
            System.out.println("Nilai n <= 5");
        }

        // Kondisi Operasional
        Integer m = 8;
        if ( n > 8 && m < 7) {
            System.out.println("Nilai n > 8 dan m < 7");
        } else {
            System.out.println("Nilai n <= 8 dan m >= 7");
        }

        // Switch
        Integer c = 3;
        switch (c) {
            case 1:
                System.out.println("nilai 1");
                break;
            case 2:
                System.out.println("nilai 2");
                break;
            case 3:
                System.out.println("nilai 3");
                break;
            default:
                System.out.println("default");
                break;
        }

    }
}
