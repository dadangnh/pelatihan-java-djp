package com.company;

public class Employee {
    // variables
    private int id;
    private String fullName;
    private String email;

    // setter/getter
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    //constructor
    // default constructor
    public Employee() {

    }
    public Employee(int id){
        this.id = id;
    }
    // deconstructor
    public void finalize() {
        System.out.println("finalize() was called");
    }

    //methods
    public void show() {
        System.out.println(this.id + "-" + this.fullName + "-" + this.email);
    }
}
