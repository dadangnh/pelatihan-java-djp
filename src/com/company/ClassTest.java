package com.company;

public class ClassTest {
    public static void main(String[] args) {
        Employee obj = new Employee(10);
        // set values
//        obj.setId(10);
        obj.setFullName("Employee 1");
        obj.setEmail("employee1@email.com");

        // get values
        System.out.println(obj.getId() + "-" + obj.getFullName() + "-" + obj.getEmail());

        // dimatikan dulu, tetapi wajib ada
//        obj.show(); // call class methods
//        obj = null;
//        System.gc();


        // sambungkan dengan office
        Office office = new Office(obj);
        System.out.println(office.getDirectorName());

        Employee emp = new Employee(5);
        emp.setFullName("Employee 2");

        if (office.absen(emp)) {
            System.out.println("Absen pegawai: " + emp.getFullName() + " berhasil");
        } else {
            System.out.println("Absen pegawai: " + emp.getFullName() + " gagal");
        }
    }
}
