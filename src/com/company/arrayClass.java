package com.company;

public class arrayClass {
    public static void main(String[] args) {
        // array & collection dalam java
        // array dapat ditentukan jumlah isinya, apabila lebih dari yg ditentukan, akan trigger error
        int[] numbers = new int[10];
        // atau bisa langsung diisi seperti pada bahasa lainnya
        int[] bilangan = new int[]{3, 5, 6, 10};
        int[] nums = {5, 6, 7, 8, 10};
        String[] cities = {"Jakarta", "Jogja", "Bandung", "Semarang", "Surabaya"};

        // set nilai
        for (int i = 0; i < numbers.length; i++) {
            numbers[i] = i + 5;
        }

        // show data
        // cara 1: menggunakan for akan urut berdasarkan index data
        for (int i = 0; i < numbers.length; i++) {
            System.out.print(numbers[i] + " ");
        }
        System.out.println();

        // cara 2: enchanted for, lebih cepat
        for (int number:numbers) {
            System.out.print(number + " ");
        }
        System.out.println();

        for (int bil:bilangan) {
            System.out.print(bil + " ");
        }
        System.out.println();

        // menampilkan string
        for (String kota:cities) {
            System.out.println("Kota sekarang: " + kota);
        }

        Employee[] employees = new Employee[10];
        for (int i = 0; i < numbers.length; i++) {
            employees[i] = new Employee();
            employees[i].setId(i + 1);
        }
        for (Employee emp:employees) {
            System.out.println(emp.getId() + " : " + emp.getFullName() + "---");
        }


    }
}
