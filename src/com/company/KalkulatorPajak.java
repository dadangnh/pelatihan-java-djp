package com.company;

import java.util.Scanner;

public class KalkulatorPajak {
    public static void main(String[] args) {
        // Ambil input dari user
        // input 1: jumlah pembayaran/ penghasilan

        Double tarifPajak = 0.0;
        String dapatDiskon = "Anda tidak mendapatkan diskon pajak";
        Boolean jalankan = true;
        Double totalPajak = 0.0;
        Integer limitPajak = 500000;

        System.out.println("==================================================");
        System.out.println("Selamat datang di simulasi");
        System.out.println("==================================================");
        System.out.println("Tarif Pajak: A = 10%, B = 25%, C = 50%");
        System.out.println("==================================================");


        // User diberi opsi untuk melanjutkan perhitungan atau berhenti
        // Apabila total pajak yang dibayar lebih dari limit pajak, maka otomatis berhenti
        do {
            if (totalPajak < limitPajak) {
                Scanner scanner = new Scanner(System.in);
                System.out.print("Masukkan jumlah pembayaran/ penghasilan Anda: ");
                Integer pembayaran = scanner.nextInt();

                // Logic perhitungan
                Boolean lanjutLoop = true;
                do {
                    System.out.print("Masukkan Tipe Pajak Anda [A, B, C]: ");
                    String tipe = scanner.next();
                    if (tipe.equals("A")) {
                        tarifPajak = 0.1;
                        lanjutLoop = false;
                    } else if (tipe.equals("B")) {
                        tarifPajak = 0.25;
                        lanjutLoop = false;
                    } else if (tipe.equals("C")) {
                        tarifPajak = 0.5;
                        lanjutLoop = false;
                    }
                } while (lanjutLoop);


                // hitung junmlah pajak yang dibayarkan
                Double jumlahPajak = tarifPajak * pembayaran;

                if (jumlahPajak >= limitPajak) {
                    dapatDiskon = "Anda mendapatkan diskon pajak";
                }

                totalPajak = totalPajak + jumlahPajak;

                // Tampilkan dua digit dibelakang koma
                // %f = float, decimal, double
                // %s = string
                // %d = integer
                System.out.println(String.format("%.2f", jumlahPajak));

                // Tampilkan ke user
                System.out.println("Total Pajak Anda adalah: " + String.format("%.2f", jumlahPajak)
                        + " Dihitung dari : " + tarifPajak  + " * " + pembayaran);
                System.out.println(dapatDiskon);

                // Akumulasi
                System.out.println("Akumulasi Pembayaran Pajak: " + String.format("%.2f", totalPajak));

                // tanyakan apakah mau hitung lagi atau enggak
                System.out.println("Apakah masih mau lanjut? [true, false]: ");
                Boolean lanjut = scanner.nextBoolean();
                if (!lanjut) {
                    jalankan = false;
                }
            } else {
                jalankan = false;
            }
        } while (jalankan);

        // Tampilkan akhirnya
        System.out.println("==================================================");
        System.out.println("Jumlah total Pajak: " + String.format("%.2f", totalPajak));
    }
}
