package com.company;

public class Office {
    // cara menghubungkan dengan class lain

    // cara 1: menjadikan variabel
    private Employee director;

    public String getDirectorName() {
        // ambil full name dari employee
        return director.getFullName();
    }

    // cara 2: digunakan sebagai parameter (seperti dependency injection_
    public Office(Employee director) {
        this.director = director;
    }

    // cara 3: sebagai method
    public boolean absen(Employee o) {
        System.out.println("Data " + o.getFullName() + " disimpan");
        // do something


        return true;
    }
}
