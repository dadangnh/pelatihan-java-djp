package com.company;

public class LoopingTest {
    public static void main(String[] args) {
        // looping for dan while

        Integer i;
        for (i = 0; i < 10; i++ ) {
            for (Integer j = 0; j < 10; j++) {
                if (j <= i) {
                    System.out.print(j);
                }
            }
            System.out.println();
        }

        Integer n = 0;
        while (n < 10) {
            System.out.println(n);
            n++;
        }
    }
}
